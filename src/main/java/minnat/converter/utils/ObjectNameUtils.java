package minnat.converter.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

public class ObjectNameUtils {

  private static Logger LOG = LoggerFactory.getLogger(ObjectNameUtils.class);

  public static final String getUnescapedObjectName(String objectName) throws UnsupportedEncodingException {
    LOG.debug("input: {}", objectName);
    String output = URLDecoder.decode(objectName, "UTF-8");
    LOG.debug("output: {}", output);
    return output;
  }

}
