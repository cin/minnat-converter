package minnat.converter.config;

public class ProcessingConfig {
  private String filter, convertedSuffix, workingDirectory;

  public String getFilter() {
    return filter;
  }

  public void setFilter(String filter) {
    this.filter = filter;
  }

  public String getConvertedSuffix() {
    return convertedSuffix;
  }

  public void setConvertedSuffix(String convertedSuffix) {
    this.convertedSuffix = convertedSuffix;
  }

  public String getWorkingDirectory() {
    return workingDirectory;
  }

  public void setWorkingDirectory(String workingDirectory) {
    this.workingDirectory = workingDirectory;
  }

  @Override
  public String toString() {
    return "ProcessingConfig{" +
        "filter='" + filter + '\'' +
        ", convertedSuffix='" + convertedSuffix + '\'' +
        ", workingDirectory='" + workingDirectory + '\'' +
        '}';
  }
}
