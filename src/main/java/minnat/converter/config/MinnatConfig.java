package minnat.converter.config;

import io.micronaut.context.annotation.ConfigurationBuilder;
import io.micronaut.context.annotation.ConfigurationProperties;

@ConfigurationProperties("minnat")
public class MinnatConfig {

  @ConfigurationBuilder(configurationPrefix = "nats-config")
  private NatsConfig natsConfig = new NatsConfig();
  @ConfigurationBuilder(configurationPrefix = "minio-config")
  private MinioConfig minioConfig = new MinioConfig();
  @ConfigurationBuilder(configurationPrefix = "processing-config")
  private ProcessingConfig processingConfig = new ProcessingConfig();

  public NatsConfig getNatsConfig() {
    return natsConfig;
  }

  public void setNatsConfig(NatsConfig natsConfig) {
    this.natsConfig = natsConfig;
  }

  public MinioConfig getMinioConfig() {
    return minioConfig;
  }

  public void setMinioConfig(MinioConfig minioConfig) {
    this.minioConfig = minioConfig;
  }

  public ProcessingConfig getProcessingConfig() {
    return processingConfig;
  }

  public void setProcessingConfig(ProcessingConfig processingConfig) {
    this.processingConfig = processingConfig;
  }

  @Override
  public String toString() {
    return "MinnatConfig{" +
        "natsConfig=" + natsConfig +
        ", minioConfig=" + minioConfig +
        ", processingConfig=" + processingConfig +
        '}';
  }
}
