package minnat.converter.config;

public class NatsConfig {
  private String host, clientId, stanClusterId, subject, queue, durableName;

  public String getHost() {
    return host;
  }

  public void setHost(String host) {
    this.host = host;
  }

  public String getClientId() {
    return clientId;
  }

  public void setClientId(String clientId) {
    this.clientId = clientId;
  }

  public String getStanClusterId() {
    return stanClusterId;
  }

  public void setStanClusterId(String stanClusterId) {
    this.stanClusterId = stanClusterId;
  }

  public String getSubject() {
    return subject;
  }

  public void setSubject(String subject) {
    this.subject = subject;
  }

  public String getQueue() {
    return queue;
  }

  public void setQueue(String queue) {
    this.queue = queue;
  }

  public String getDurableName() {
    return durableName;
  }

  public void setDurableName(String dureableName) {
    this.durableName = dureableName;
  }

  @Override
  public String toString() {
    return "NatsConfig{" +
        "host='" + host + '\'' +
        ", clientId='" + clientId + '\'' +
        ", stanClusterId='" + stanClusterId + '\'' +
        ", subject='" + subject + '\'' +
        ", queue='" + queue + '\'' +
        ", durableName='" + durableName + '\'' +
        '}';
  }
}
