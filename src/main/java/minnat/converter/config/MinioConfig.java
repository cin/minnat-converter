package minnat.converter.config;

public class MinioConfig {
  private String host;
  private String accessKey;
  private String secretKey;

  public String getHost() {
    return host;
  }

  public void setHost(String host) {
    this.host = host;
  }

  public String getAccessKey() {
    return accessKey;
  }

  public void setAccessKey(String accessKey) {
    this.accessKey = accessKey;
  }

  public String getSecretKey() {
    return secretKey;
  }

  public void setSecretKey(String secretKey) {
    this.secretKey = secretKey;
  }

  @Override
  public String toString() {
    return "MinioConfig{" +
        "host='" + host + '\'' +
        ", accessKey='" + accessKey + '\'' +
        ", secretKey='" + secretKey + '\'' +
        '}';
  }
}
