package minnat.converter.services;

import io.micronaut.context.annotation.Prototype;
import net.bramp.ffmpeg.FFmpeg;
import net.bramp.ffmpeg.FFmpegExecutor;
import net.bramp.ffmpeg.FFmpegUtils;
import net.bramp.ffmpeg.FFprobe;
import net.bramp.ffmpeg.builder.FFmpegBuilder;
import net.bramp.ffmpeg.job.FFmpegJob;
import net.bramp.ffmpeg.probe.FFmpegProbeResult;
import net.bramp.ffmpeg.progress.Progress;
import net.bramp.ffmpeg.progress.ProgressListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

@Prototype
public class VideoConverter {

  private static final Logger LOG = LoggerFactory.getLogger(VideoConverter.class);

  public void convertVideoTo720p(String sourceFilePath, String outputFilePath) throws IOException {
    LOG.info("converting {}",sourceFilePath);

    FFmpeg ffmpeg = new FFmpeg("/usr/bin/ffmpeg");
    FFprobe ffprobe = new FFprobe("/usr/bin/ffprobe");

    FFmpegProbeResult in = ffprobe.probe(sourceFilePath);

    FFmpegBuilder builder = new FFmpegBuilder()
        //.addExtraArgs("-hwaccel", "vaapi", "-hwaccel_device", "/dev/dri/renderD128")
        .setInput(sourceFilePath)
        .overrideOutputFiles(false)

        .addOutput(outputFilePath)
        .setFormat("mp4")

        .disableSubtitle()

        .setAudioCodec("copy")

        .setVideoMovFlags("faststart")
        .setVideoCodec("h264")
        .setVideoFrameRate(30, 1)
        .setVideoResolution(1280, 720)

        .setStrict(FFmpegBuilder.Strict.EXPERIMENTAL)
        .done();

    FFmpegExecutor executor = new FFmpegExecutor(ffmpeg, ffprobe);

    FFmpegJob job = executor.createJob(builder, new ProgressListener() {
      final double duration_ns = in.getFormat().duration * TimeUnit.SECONDS.toNanos(1);

      @Override
      public void progress(Progress progress) {
        double percentage = progress.out_time_ns / duration_ns;

        // Print out interesting information about the progress
        LOG.info(String.format(
            "[%.0f%%] status:%s frame:%d time:%s ms fps:%.0f speed:%.2fx",
            percentage * 100,
            progress.status,
            progress.frame,
            FFmpegUtils.toTimecode(progress.out_time_ns, TimeUnit.NANOSECONDS),
            progress.fps.doubleValue(),
            progress.speed
        ));
      }
    });
    job.run();
  }

}
