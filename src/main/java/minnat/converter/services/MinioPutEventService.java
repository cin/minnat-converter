package minnat.converter.services;

import io.micronaut.scheduling.annotation.Async;
import io.minio.messages.Event;
import minnat.converter.config.MinnatConfig;
import minnat.converter.utils.ObjectNameUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.management.ObjectName;
import java.io.IOException;

@Singleton
public class MinioPutEventService {

  private static final Logger LOG = LoggerFactory.getLogger(MinioPutEventService.class);

  @Inject
  MinnatConfig config;

  @Inject
  private MinioFileService fileService;

  @Inject
  private VideoConverter converter;

  @Async
  public void handleNewFilePutEvent(Event event) {
    try {
      String objectName = ObjectNameUtils.getUnescapedObjectName(event.objectName());

      LOG.info("Got event {} for file {} starting async processing", event.eventType(), objectName);

      String inputFileFullPath = fileService.downloadFile(event);
      String outputFileFullPath = getOutputFilePath(inputFileFullPath);
      converter.convertVideoTo720p(inputFileFullPath, outputFileFullPath);

      fileService.uploadFile(event.bucketName(), outputFileFullPath, getOutputFilePath(objectName));

      fileService.cleanupWorkingDir(inputFileFullPath, outputFileFullPath);

      LOG.info("Finished processing {} for file {}", event.eventType(), objectName);
    } catch (IOException e) {
      LOG.error("IOException", e);
    } catch (Exception e) {
      LOG.error("Exception", e);
    }

  }

  private String getOutputFilePath(String inputFileName) {
    return inputFileName.replace(config.getProcessingConfig().getFilter(),
        config.getProcessingConfig().getConvertedSuffix()+config.getProcessingConfig().getFilter());
  }

}
