package minnat.converter.services;

import io.micronaut.context.annotation.Prototype;
import io.minio.MinioClient;
import io.minio.errors.*;
import io.minio.messages.Event;
import minnat.converter.utils.ObjectNameUtils;
import minnat.converter.config.MinnatConfig;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

@Prototype
public class MinioFileService {

  private static final Logger LOG = LoggerFactory.getLogger(MinioFileService.class);

  @Inject
  MinnatConfig config;

  MinioClient minioClient;

  @PostConstruct
  private void init() throws InvalidPortException, InvalidEndpointException {
    minioClient = new MinioClient(
        config.getMinioConfig().getHost(),
        config.getMinioConfig().getAccessKey(),
        config.getMinioConfig().getSecretKey()
    );
  }


  public String downloadFile(Event event) throws Exception {
    LOG.info("downloading {}", event.objectName());
    try {
      LOG.info("try to download file from event {}", event);
      boolean isExists = minioClient.bucketExists(event.bucketName());
      if(isExists) {
        String objectName = ObjectNameUtils.getUnescapedObjectName(event.objectName());

        InputStream inputStream = minioClient.getObject(event.bucketName(), objectName);
        String targetFileFullPath = getSourceFileFullPath(event);
        File file = new File(targetFileFullPath);
        FileUtils.copyInputStreamToFile(inputStream, file);

        inputStream.close();
        return targetFileFullPath;
      }
    } catch (InvalidKeyException e) {
      LOG.error("InvalidKeyException", e);
    } catch (NoSuchAlgorithmException e) {
      LOG.error("NoSuchAlgorithmException", e);
    } catch (InvalidResponseException e) {
      LOG.error("InvalidResponseException", e);
    } catch (InvalidBucketNameException e) {
      LOG.error("InvalidBucketNameException", e);
    } catch (ErrorResponseException e) {
      LOG.error("ErrorResponseException", e);
    } catch (InsufficientDataException e) {
      LOG.error("InsufficientDataException", e);
    } catch (XmlParserException e) {
      LOG.error("XmlParserException", e);
    } catch (InternalException e) {
      LOG.error("InternalException", e);
    } catch (IOException e) {
      LOG.error("IOException", e);
    }
    throw new Exception("something is really strange, do something here like a cleanup or a message");
  }

  public void uploadFile(String bucket, String path, String fileName) {
    LOG.info("uploading {}", fileName);

    try {
      minioClient.putObject(bucket, fileName, path, null);
    } catch (InvalidBucketNameException e) {
      LOG.error("InvalidBucketNameException", e);
    } catch (NoSuchAlgorithmException e) {
      LOG.error("NoSuchAlgorithmException", e);
    } catch (IOException e) {
      LOG.error("IOException", e);
    } catch (InvalidKeyException e) {
      LOG.error("InvalidKeyException", e);
    } catch (XmlParserException e) {
      LOG.error("XmlParserException", e);
    } catch (ErrorResponseException e) {
      LOG.error("ErrorResponseException", e);
    } catch (InternalException e) {
      LOG.error("InternalException", e);
    } catch (InsufficientDataException e) {
      LOG.error("InsufficientDataException", e);
    } catch (InvalidResponseException e) {
      LOG.error("InvalidResponseException", e);
    }
  }

  public void cleanupWorkingDir(String inputFileFullPath, String outputFileFullPath) {
    File sourceFile = new File(inputFileFullPath);
    LOG.info("Clean up old input file {}", inputFileFullPath);
    sourceFile.delete();

    File targetFile = new File(outputFileFullPath);
    LOG.info("Clean up old output file {}", outputFileFullPath);
    targetFile.delete();
  }

  private String getSourceFileFullPath(Event event) throws UnsupportedEncodingException {
    return config.getProcessingConfig().getWorkingDirectory()+"/"+ObjectNameUtils.getUnescapedObjectName(event.objectName());
  }
}
