package minnat.converter.usecases;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.micronaut.context.annotation.Context;
import io.minio.messages.NotificationRecords;
import io.nats.streaming.*;
import minnat.converter.config.MinnatConfig;
import minnat.converter.services.MinioPutEventService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.inject.Inject;
import java.io.*;
import java.net.InetAddress;
import java.util.concurrent.TimeoutException;

@Context
public class NatsStreamEventListener {

  private static final Logger LOG = LoggerFactory.getLogger(NatsStreamEventListener.class);

  @Inject
  MinnatConfig config;

  @Inject
  public MinioPutEventService manager;

  @PostConstruct
  public void initialize() throws InterruptedException, TimeoutException, IOException {
    LOG.info("initializing");
    LOG.info(config.toString());
    initListener();
  }

  private Subscription sub;
  private StreamingConnection sc;

  void initListener() throws IOException, InterruptedException, TimeoutException {
    StreamingConnectionFactory cf = new StreamingConnectionFactory(new Options.Builder()
        .clientId(config.getNatsConfig().getClientId()+ InetAddress.getLocalHost().getHostName())
        .clusterId(config.getNatsConfig().getStanClusterId())
        .natsUrl(config.getNatsConfig().getHost())
        .build());
    sc = cf.createConnection();

    sub = sc.subscribe(config.getNatsConfig().getSubject(), config.getNatsConfig().getQueue(), new MessageHandler() {
      @Override
      public void onMessage(Message msg) {
        try {
          ObjectMapper mapper = new ObjectMapper();
          mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
          NotificationRecords records = mapper.readValue(msg.getData(), NotificationRecords.class);

          records.events().stream()
              .filter(event -> isConversionNeeded(event.objectName()))
              .forEach((event -> {
            manager.handleNewFilePutEvent(event);
          }));
        } catch (IOException e) {
          LOG.error("error converting message to event", e);
        }
      }
    }, new SubscriptionOptions.Builder()
        .durableName(config.getNatsConfig().getDurableName())
        .build());
  }

  @PreDestroy
  public void destroy() throws IOException, TimeoutException, InterruptedException {
    sub.unsubscribe();
    sc.close();
  }

  private boolean isConversionNeeded(String objectName) {
    return objectName.endsWith(config.getProcessingConfig().getFilter()) &&
        !objectName.endsWith(config.getProcessingConfig().getConvertedSuffix()+config.getProcessingConfig().getFilter());
  }
}
