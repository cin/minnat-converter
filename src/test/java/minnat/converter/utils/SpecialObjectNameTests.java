package minnat.converter.utils;

import minnat.converter.utils.ObjectNameUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.UnsupportedEncodingException;

public class SpecialObjectNameTests {


  @Test
  void objectConversion() throws UnsupportedEncodingException {
    String name = "Orgelgru%C3%9F+Chor+20.04..mp4";

    String unescaped = ObjectNameUtils.getUnescapedObjectName(name);

    Assertions.assertEquals("Orgelgruß Chor 20.04..mp4", unescaped);
  }

}
