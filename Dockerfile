FROM openjdk:11.0-jre-slim

RUN apt-get update && \
    apt-get install -y ffmpeg && \
    rm -rf /var/lib/apt/lists/*

COPY build/libs/minnat-converter-*-all.jar minnat-converter.jar
EXPOSE 8080
CMD ["java", "-Dcom.sun.management.jmxremote", "-Xmx256m", "-jar", "minnat-converter.jar"]
